# AnyHedge Whitepaper

[Whitepaper](./anyhedge.md) ([pdf](./anyhedge.pdf))

[Supplement: A detailed example execution of AnyHedge on Bitcoin Cash](./anyhedge-supplement.md) ([pdf](./anyhedge-supplement.pdf))
